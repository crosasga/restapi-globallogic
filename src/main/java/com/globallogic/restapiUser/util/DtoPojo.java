package com.globallogic.restapiUser.util;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import com.globallogic.restapiUser.dto.PhoneDTO;
import com.globallogic.restapiUser.dto.UserDTO;
import com.globallogic.restapiUser.model.Phones;
import com.globallogic.restapiUser.model.Users;

public class DtoPojo {

  
  /*Metodo para convertir dto user a model*/
  public static Users convert(UserDTO userDTO) {
    Users users = new Users();
    //Collection<Phones> phones = new ArrayList <>();
    List<Phones>  phones= new ArrayList <>();
    users.setName(userDTO.getName());
    users.setEmail(userDTO.getEmail());
    users.setPassword(userDTO.getPassword());
    users.setToken(userDTO.getToken());
    userDTO.getPhoneDTOs().forEach(phonesDTO ->phones.add(convertPhone(phonesDTO,users))); //expresion lambda java 8
    //users.setPhones(phones);
    return users;
  }
  
  /*Metodo aux  para convertir phones en model phone */
  public static Phones convertPhone(PhoneDTO phoneDTO, Users users) {
    Phones phones = new Phones();
    phones.setNumber(phoneDTO.getNumber());
    phones.setCountryCode(phoneDTO.getCountryCode());
    phones.setCityCode(phoneDTO.getCityCode());
   // phones.setUsersId(users.getId());
    phones.setUsers(users);
    return phones;
  }
  
  
  public static Collection<Phones> addPhoneToUSer(PhoneDTO phone, Users users) {
    Collection<Phones> phones = new ArrayList <>();
    users.getPhones().forEach(phonesDTO -> phones.add(convertPhone(phone,users)));
    return phones;
  }
}
