package com.globallogic.restapiUser.model;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import org.hibernate.annotations.GenericGenerator;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
@Entity
@Table(name ="users")
public class Users {
/*
  @Id
  private String id= UUID.randomUUID().toString();
  */
  @Id
  @GeneratedValue(generator = "uuid2")
  @GenericGenerator(name = "uuid2", strategy = "uuid2")
  @Column(name = "id", columnDefinition = "BINARY(16)")
  private UUID id;
  
  @Column(nullable = false)
  private String name;
  
  @Column(nullable = false)
  @NotEmpty(message = "email can not be empty.")
  @NotNull(message = "email can not be null.")
  @Email(message ="Debe ser un email valido")
  private String email;
  
  @Column(nullable = false)
  private String password;
  
  @Column(nullable = false)
  private LocalDateTime created = LocalDateTime.now();
  
  @Column(nullable = false)
  private LocalDateTime modified = LocalDateTime.now();
  
  @Column(nullable = false)
  @JsonProperty(value = "last_login")
  private LocalDateTime lastLogin = LocalDateTime.now();
  
  @Column(nullable = false)
  @JsonProperty(value = "isactive")
  private Boolean isActive = true;
  
  private String token;
  
  @OneToMany(mappedBy = "users", cascade = CascadeType.ALL,orphanRemoval = true)
  private List<Phones> phones = new ArrayList<>();
  
}
