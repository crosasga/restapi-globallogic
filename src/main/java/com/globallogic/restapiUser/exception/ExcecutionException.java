package com.globallogic.restapiUser.exception;

import java.util.List;
import org.springframework.http.HttpStatus;
import com.fasterxml.jackson.annotation.JsonProperty;

public class ExcecutionException {

  /**
   * 
   */
  @JsonProperty("status_code")
  private HttpStatus status;
  
  @JsonProperty("message")
  private String mensaje;
  
  @SuppressWarnings("unused")
  private List<String> errors;
  
  @JsonProperty("uri")
  private String uriRequested;
  
  public ExcecutionException(String mensaje) { 

    this.mensaje = mensaje;
  }
  
  
  public ExcecutionException(HttpStatus status, String mensaje) {
   // super();
    this.status = status;
    this.mensaje = mensaje;
  }
 


  public ExcecutionException(HttpStatus statusCode, String message, String uriRequested) {
  //  super();
    this.mensaje = message;
    this.status = statusCode;
    this.uriRequested = uriRequested;
  }
}
