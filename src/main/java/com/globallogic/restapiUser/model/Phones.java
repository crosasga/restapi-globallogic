package com.globallogic.restapiUser.model;

import java.util.UUID;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import org.hibernate.annotations.GenericGenerator;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
@Entity
@Table(name ="phones")
public class Phones {

  @Id
  @GeneratedValue(generator = "uuid2")
  @GenericGenerator(name = "uuid2", strategy = "uuid2")
  @Column(name = "id", columnDefinition = "BINARY(16)")
  private UUID id;
  
   
  @JsonProperty(value = "number")
  @Column(nullable = false)
  private String number;
  
  
  @JsonProperty(value = "citycode")
  @Column(nullable = false)
  private String cityCode;
  
  @JsonProperty(value = "contrycode")
  @Column(nullable = false)
  private String countryCode;
  
  @ManyToOne(optional = false,fetch = FetchType.EAGER)
  @JoinColumn(name ="users_id", nullable = false)
  @JsonIgnoreProperties("users")
  private Users users;
  
}
