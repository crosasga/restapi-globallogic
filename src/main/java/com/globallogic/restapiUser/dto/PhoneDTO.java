package com.globallogic.restapiUser.dto;

import javax.validation.constraints.NotNull;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.globallogic.restapiUser.constants.Message;
import lombok.Data;

@Data
public class PhoneDTO {

  @NotNull(message = "Numero "+Message.MESSAGE_NO_NULL)
  private String number;
  
  @NotNull(message =  "cityCode "+Message.MESSAGE_NO_NULL)
  @JsonProperty(value = "citycode")
  private String cityCode;
  
  @JsonProperty(value = "contrycode")
  @NotNull(message = "contrycode "+ Message.MESSAGE_NO_NULL)
  private String countryCode;
  
}
