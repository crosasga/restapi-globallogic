package com.globallogic.restapiUser.exception.advice;

import javax.servlet.http.HttpServletRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import com.globallogic.restapiUser.exception.ExcecutionException;
import com.globallogic.restapiUser.exception.ExcecutionExceptionMessage;

@ControllerAdvice
public class ErrorHandler {

   @ExceptionHandler(MethodArgumentNotValidException.class)
   public ResponseEntity<ExcecutionException> methodArgumentNotValidException(HttpServletRequest request, MethodArgumentNotValidException e) {
     ExcecutionException errorInfo = new ExcecutionException(HttpStatus.BAD_REQUEST, e.getMessage(), request.getRequestURI());
       return new ResponseEntity<>(errorInfo, HttpStatus.BAD_REQUEST);
   }
   @ExceptionHandler(Exception.class)
   public ResponseEntity<ExcecutionExceptionMessage> eException(HttpServletRequest request, Exception e) {
     //ExcecutionException errorInfo = new ExcecutionException(HttpStatus.BAD_REQUEST, e.getMessage(), request.getRequestURI());
     ExcecutionExceptionMessage errorMessage = new ExcecutionExceptionMessage(e.getMessage());
     return new ResponseEntity<>(errorMessage, HttpStatus.BAD_REQUEST);
   }
}