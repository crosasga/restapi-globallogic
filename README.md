# Proyecto Ejercicio Registro usuario (GlobalLogic)


Proyecto microservicio spring boot java con junit para disponiblizar servicio donde se acepta y retona solamente json. Se utilizo IDE STS 4.7.0, java 1.8, junit 5, gradle

## Installation

Realizar clone 

```bash
 git clone https://gitlab.com/crosasga/restapi-globallogic.git
```

## Diagrama Secuencia

Diagrama de secuencia

![img](documentacion/xmlGlobal-xml_secuencia.png)

## Diagrama Clases
Diagrama de componentes (principales clases)

![img](documentacion/principal_class.png)

## Requerimiento

```
Todos los mensaje debe ser con formato:
{"mensaje": "mensaje de error"}


```
Formato de entrada
```
{
 "name": "Juan Rodriguez",
 "email": "juan@rodriguez.org",
 "password": "hunter2",
 "phones": [
    {
    "number": "1234567",
    "citycode": "1",
    "contrycode": "57"
    }
   ]
}

```
-  Responder el código de status HTTP adecuado
-  En caso de éxito, retorne el usuario y los siguientes campos:
-  id: id del usuario (puede ser lo que se genera por el banco de datos, pero sería más
deseable un UUID)
	 - created: fecha de creación del usuario
	 -  modified: fecha de la última actualización de usuario
	 -  last_login: del último ingreso (en caso de nuevo usuario, va a coincidir con la fecha
de creación)
	 -  token: token de acceso de la API (puede ser UUID o JWT)
	 - isactive: Indica si el usuario sigue habilitado dentro del sistema.
-  Si caso el correo conste en la base de datos, deberá retornar un error "El correo ya
registrado".
-  El correo debe seguir una expresión regular para validar que formato sea el correcto.
(aaaaaaa@dominio.cl)
-  La clave debe seguir una expresión regular para validar que formato sea el correcto. (Una
Mayúscula, letras minúsculas, y dos números)
-  Se debe hacer traza de logs dentro del aplicativo.


## Requisitos Mandatorios
Plazo: 2 días.
 - Banco de datos en memoria.
 -  Gradle como herramienta de construcción.
 - Pruebas unitarias (Deseable: Spock Framework).
 - Persistencia con Hibernate.
 - Framework Spring Boot.
 - Java 8 o superior. (Usar más de dos características propias de la versión)
 - Entrega en un repositorio público (github, gitlab o bitbucket) con el código fuente.
 -  Entregar diagrama de componentes de la solución y al menos un diagrama de secuencia
(ambos diagramas son de carácter obligatorio y deben seguir estándares UML).
 -  README.md debe contener las instrucciones para levantar y usar el proyecto.



## License
[MIT](https://choosealicense.com/licenses/mit/)