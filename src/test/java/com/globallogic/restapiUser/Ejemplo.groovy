package com.globallogic.restapiUser

import static org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.test.web.servlet.MockMvc

class Ejemplo {

  @Autowired
  private MockMvc mvc
  
  @Autowired
  private RestapiUserApplication restapiWeb;
  
  def "when get is performed then the response has status 200 and content is 'Hello world!'"() {
    given:
  
    
    expect: "Status is 200 and the response is 'Hello world!'"
    mvc.perform(get("/hello"))
      .andExpect(status().isOk())
      .andReturn()
      .response
      .contentAsString == "Hello world!"
   then:
      result == true
}
  
  
  @Test
  void test() {
   
   
    
  fail("Not yet implemented")
   }


}
