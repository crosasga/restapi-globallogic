package com.globallogic.restapiUser.exception;

import com.fasterxml.jackson.annotation.JsonProperty;

public class ExcecutionExceptionMessage {

  
  @JsonProperty("message")
  private String mensaje;
  
   public ExcecutionExceptionMessage(String mensaje) { 

    this.mensaje = mensaje;
  }
  
  
}
