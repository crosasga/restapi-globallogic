package com.globallogic.restapiUser.controller.response;

public abstract class GlobalReponse {
  protected String state;

  public abstract String getState();
}
