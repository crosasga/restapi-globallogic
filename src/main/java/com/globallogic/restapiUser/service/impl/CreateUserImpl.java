package com.globallogic.restapiUser.service.impl;

import java.util.Collection;
import javax.transaction.Transactional;
import org.springframework.stereotype.Service;
import com.globallogic.restapiUser.constants.LoggerMessages;
import com.globallogic.restapiUser.dto.PhoneDTO;
import com.globallogic.restapiUser.dto.UserDTO;
import com.globallogic.restapiUser.model.Users;
import com.globallogic.restapiUser.service.CreateUser;
import com.globallogic.restapiUser.service.PhoneService;
import com.globallogic.restapiUser.service.UserService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
@AllArgsConstructor
public class CreateUserImpl  implements CreateUser {

  private PhoneService phoneServiceImpl;
  private UserService userServiceImpl;
  
  @Transactional
  public Users create(UserDTO userDTO) throws  Exception{
    log.trace(LoggerMessages.TRACE_METHOD_ENTERING, "create User");
    log.info("Metodo Create on CreateUserImpl",userDTO);
    userServiceImpl.usuarioExiste(userDTO.getEmail());
    Users user;
   
      user= userServiceImpl.save(userDTO);
      phoneServiceImpl.save(user.getPhones(), user);
   
    
    log.trace(LoggerMessages.TRACE_METHOD_LEAVING, "create User");
    return user;
    
  }

  @Override
  public Users create(UserDTO userDTO, Collection<PhoneDTO> phoneDTOs) throws Exception {
    // TODO Auto-generated method stub
    Users user= create(userDTO);
    return user;
  }
}
