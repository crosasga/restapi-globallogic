package com.globallogic.restapiUser.constants;

public class Message {
  
  public static final String MESSAGE_DEFAULT = "mensaje";
  public static final String MESSAGE_NO_NULL = "No puede ser nulo";
  public static final String MESSAGE_EMPTY = "No puede ser vacio";
  public static final String MESSAGE_EMAIL_FORMAT = "Password debe tener 1 Mayuscula,letras minusculas , dos numeros";
  public static final String MESSAGE_USER_EXISTS = "El correo ingresado ya se encuentra registrado";
  public static final String MESSAGE_PHONES_SIZE = "Debe ingresar telefono asciados a su correo";
  public static final String MESSAGE_GENERICO = "Ocurrio alguna excepcion";
}
