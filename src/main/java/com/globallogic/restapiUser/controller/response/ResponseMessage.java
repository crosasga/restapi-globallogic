package com.globallogic.restapiUser.controller.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
@Getter
public class ResponseMessage {
  @JsonProperty("message")
  private String message;

  public ResponseMessage(String message) {

    this.message = message;

  }
}
