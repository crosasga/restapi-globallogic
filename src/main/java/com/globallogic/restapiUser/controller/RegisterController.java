package com.globallogic.restapiUser.controller;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import com.globallogic.restapiUser.constants.LoggerMessages;
import com.globallogic.restapiUser.constants.Message;
import com.globallogic.restapiUser.controller.response.ResponseMessage;
import com.globallogic.restapiUser.dto.UserDTO;
import com.globallogic.restapiUser.model.Phones;
import com.globallogic.restapiUser.model.Users;
import com.globallogic.restapiUser.service.CreateUser;
import lombok.AllArgsConstructor;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

@RestController
@AllArgsConstructor
// @RequestMapping(path = "/authorization", produces = MediaType.APPLICATION_JSON_VALUE)
public class RegisterController {

  private static final Logger LOGGER = LoggerFactory.getLogger(RegisterController.class);
  private final CreateUser createUser;

  /** ]Controller register path and function. */
  @PostMapping(path = "/register", consumes = MediaType.APPLICATION_JSON_VALUE, 
      produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity<?> register(@NotNull(message = Message.MESSAGE_NO_NULL)
      @RequestBody @Valid final UserDTO userDTO,  final BindingResult results) throws Exception {
     
    LOGGER.info(LoggerMessages.TRACE_METHOD_ENTERING, "register");
    ResponseMessage responseOK;
      if (results.hasErrors()) {
        ObjectError error;
        int errorLastPos;
        errorLastPos = results.getAllErrors().size() -1 ;
        error = results.getAllErrors().get(errorLastPos);
        HashMap<Object, Object> respuestaError = new HashMap<>();
        HashMap<Object, Object> soloObjeto = new HashMap<>();
        respuestaError.put(Message.MESSAGE_DEFAULT, error);
        soloObjeto.put(Message.MESSAGE_DEFAULT, results.getAllErrors().get(0).getDefaultMessage());
        LOGGER.error("Validacion de Errores ",respuestaError );
        responseOK = new ResponseMessage(results.getAllErrors().get(0).getDefaultMessage());
        return new ResponseEntity<>(responseOK, HttpStatus.PRECONDITION_REQUIRED);
      }
      LOGGER.trace(LoggerMessages.TRACE_METHOD_LEAVING, "register");
     String token = getJWTToken(userDTO.getName());
     userDTO.setToken(token);
     Users user= createUser.create(userDTO, userDTO.getPhoneDTOs());
     //user.getPhones().remove(Phones.class);
     //return   ResponseEntity.status(HttpStatus.CREATED).body(Message.MESSAGE_DEFAULT);
     // user.getPhones().remove(user);
      return new ResponseEntity<>(user, HttpStatus.CREATED);

}


  private String getJWTToken(String username) {
    String secretKey = "global";
    List<GrantedAuthority> grantedAuthorities = AuthorityUtils
            .commaSeparatedStringToAuthorityList("ROLE_USER");
    
    String token = Jwts
            .builder()
            .setId("globalJWT")
            .setSubject(username)
            .claim("authorities",
                    grantedAuthorities.stream()
                            .map(GrantedAuthority::getAuthority)
                            .collect(Collectors.toList()))
            .setIssuedAt(new Date(System.currentTimeMillis()))
            .setExpiration(new Date(System.currentTimeMillis() + 600000))
            .signWith(SignatureAlgorithm.HS512,
                    secretKey.getBytes()).compact();

    return "Bearer " + token;
}

}
