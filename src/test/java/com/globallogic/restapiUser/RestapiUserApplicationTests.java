package com.globallogic.restapiUser;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.extension.ExtendWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.context.WebApplicationContext;
import com.globallogic.restapiUser.constants.LoggerMessages;
import com.globallogic.restapiUser.dto.UserDTO;
import com.globallogic.restapiUser.generator.DataDumy;
import com.globallogic.restapiUser.utils.Utils;

@SpringBootTest
@WebAppConfiguration
@ExtendWith(SpringExtension.class)
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class RestapiUserApplicationTests {

  private MockMvc mvc;
  @Autowired
  private WebApplicationContext webApplicationContext;

  private static final Logger LOGGER = LoggerFactory.getLogger(RestapiUserApplicationTests.class);


  @BeforeAll
  public void setUp() {
    this.mvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();
  }


  @Transactional
  @Test
  public void createTestUserOK() throws Exception {
    LOGGER.trace(LoggerMessages.TRACE_METHOD_ENTERING, "createTestUserOK");
    DataDumy generador = new DataDumy();
    UserDTO userdto = generador.getRegisterAllData();
    this.mvc.perform(post("/register")
        // .contentType(MediaType.APPLICATION_JSON)
        .contentType(MediaType.APPLICATION_JSON)
        .accept(MediaType.APPLICATION_JSON)
        .content(Utils.asJsonString(userdto)))
        .andExpect(status().isCreated());
    LOGGER.trace(LoggerMessages.TRACE_METHOD_LEAVING, "createTestUserOK");

  }

  @Transactional
  @Test
  public void createTestUserNOK() throws Exception {
    LOGGER.trace(LoggerMessages.TRACE_METHOD_ENTERING, "createTestUserNOK");
    DataDumy generador = new DataDumy();
    this.mvc
        .perform(post("/register2").contentType(MediaType.APPLICATION_JSON)
        .accept(MediaType.APPLICATION_JSON).content(Utils.asJsonString(generador)))
        .andExpect(status().isNotFound());
    LOGGER.trace(LoggerMessages.TRACE_METHOD_LEAVING, "createTestUserNOK");
  }


  @Transactional
  @Test
  public void createEmailDuplicate() throws Exception {
    LOGGER.trace(LoggerMessages.TRACE_METHOD_ENTERING, "createEmailDuplicate");
    DataDumy generador = new DataDumy();
    UserDTO userdto = generador.getRegisterAllData();
    //esta generando ya con un insert en data.sql el dato con email alan.brito@gmail.com
    userdto.setEmail("alan.brito@gmail.com");
    this.mvc.perform(post("/register")
        .contentType(MediaType.APPLICATION_JSON)
        .content(Utils.asJsonString(userdto)))
        .andDo(print())
        .andExpect(status().isBadRequest())
        .andExpect(jsonPath("$.message").value("El correo ingresado ya se encuentra registrado"));
    LOGGER.trace(LoggerMessages.TRACE_METHOD_LEAVING, "createEmailDuplicate");
  }

  @Transactional
  @Test
  public void createEmailNull() throws Exception {
    LOGGER.trace(LoggerMessages.TRACE_METHOD_ENTERING, "createEmailDuplicate");
    createTestUserOK();
    DataDumy generador = new DataDumy();
    UserDTO userdto = generador.getRegisterAllData();
    userdto.setEmail(null);
    this.mvc.perform(post("/register")
        .contentType(MediaType.APPLICATION_JSON)
        .content(Utils.asJsonString(userdto)))
        .andDo(print())
        .andExpect(status().isPreconditionRequired())
        .andExpect(jsonPath("$.message").value("Email No puede ser nulo"));
    LOGGER.trace(LoggerMessages.TRACE_METHOD_LEAVING, "createEmailDuplicate");
  }
  
  @Transactional
  @Test
  public void createEmailPatern() throws Exception {
    LOGGER.trace(LoggerMessages.TRACE_METHOD_ENTERING, "createEmailPatern");
    createTestUserOK();
    DataDumy generador = new DataDumy();
    UserDTO userdto = generador.getRegisterAllData();
    userdto.setEmail("miema.org");
    this.mvc.perform(post("/register")
        .contentType(MediaType.APPLICATION_JSON)
        .content(Utils.asJsonString(userdto)))
        .andDo(print())
        .andExpect(status().isPreconditionRequired())
        .andExpect(jsonPath("$.message").value("must be a well-formed email address"));//esta en ingles aqui porque ocupa el tag @email en el model
    LOGGER.trace(LoggerMessages.TRACE_METHOD_LEAVING, "createEmailPatern");
  }
  
  @Transactional
  @Test
  public void createNameNull() throws Exception {
    LOGGER.trace(LoggerMessages.TRACE_METHOD_ENTERING, "createNameEmpty");
    createTestUserOK();
    DataDumy generador = new DataDumy();
    UserDTO userdto = generador.getRegisterAllData();
    userdto.setName(null);
    this.mvc.perform(post("/register")
        .contentType(MediaType.APPLICATION_JSON)
        .content(Utils.asJsonString(userdto)))
        .andDo(print())
        .andExpect(status().isPreconditionRequired())
        .andExpect(jsonPath("$.message").value("Nombre No puede ser nulo"));
    LOGGER.trace(LoggerMessages.TRACE_METHOD_LEAVING, "createNameEmpty");
  }
  
  @Transactional
  @Test
  public void createPasswordPatern() throws Exception {
    LOGGER.trace(LoggerMessages.TRACE_METHOD_ENTERING, "createPasswordPatern");
    createTestUserOK();
    DataDumy generador = new DataDumy();
    UserDTO userdto = generador.getRegisterAllData();
    userdto.setPassword("miema.org");
    this.mvc.perform(post("/register")
        .contentType(MediaType.APPLICATION_JSON)
        .content(Utils.asJsonString(userdto)))
        .andDo(print())
        .andExpect(status().isPreconditionRequired())
        .andExpect(jsonPath("$.message").value("Password debe tener 1 Mayuscula,letras minusculas , dos numeros"));
    LOGGER.trace(LoggerMessages.TRACE_METHOD_LEAVING, "createPasswordPatern");
  }

}
