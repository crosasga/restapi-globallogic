package com.globallogic.restapiUser.constants;

/**
 * The Class ExceptionMessages.
 */
public class ExceptionMessages {

  /**
   * Instantiates a new exception messages.
   */
  private ExceptionMessages() {
    throw new IllegalStateException(MSG_EXCEPTION_CONSTANTS);
  }

  /** The Constant MSG_EXCEPTION_CONSTANTS. */
  /* CONSTANTS CLASSES */
  public static final String MSG_EXCEPTION_CONSTANTS = "Utility class";

  /** The Constant REPORTS_NAME_HEADERS_DIF. */
  public static final String REPORTS_NAME_HEADERS_DIF = "Headers reports are diferents";
}
