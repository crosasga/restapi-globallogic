package com.globallogic.restapiUser.service.impl;

import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.globallogic.restapiUser.constants.LoggerMessages;
import com.globallogic.restapiUser.constants.Message;
import com.globallogic.restapiUser.dto.UserDTO;
import com.globallogic.restapiUser.model.Users;
import com.globallogic.restapiUser.repository.UserRepository;
import com.globallogic.restapiUser.service.UserService;
import com.globallogic.restapiUser.util.DtoPojo;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
@AllArgsConstructor
@Transactional(readOnly = false)
public class UserServiceImpl implements UserService {
  
  @Autowired
  private UserRepository userRepository;
  
  @Transactional
  public Users save(UserDTO userDTO) throws Exception{
    log.trace(LoggerMessages.TRACE_METHOD_ENTERING, "save user");
    log.info("userDTO ",  userDTO.toString());
    Users users = DtoPojo.convert(userDTO);
    log.trace(LoggerMessages.TRACE_METHOD_LEAVING, "save user");
    userRepository.save(users);
    return users;
  }
  
  public void usuarioExiste(String email) throws  Exception{
    log.trace(LoggerMessages.TRACE_METHOD_ENTERING, "usuarioNoExiste");
    log.info("email ",  email);
    Optional<Users> users = userRepository.findByEmail(email) ;// propiedade java 8 usar clase optional y luego ocupar ispresent cambiando la forma de manejar nulos
     if(users.isPresent()) {
       log.info("Usuario existe");
      throw new Exception(Message.MESSAGE_USER_EXISTS);
       //return Collections.singletonMap(Message.MESSAGE_DEFAULT, Message.MESSAGE_USER_EXISTS);
     }
      log.trace(LoggerMessages.TRACE_METHOD_LEAVING, "usuarioNoExiste");
  }

}
